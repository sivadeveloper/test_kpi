WITH METRIC as (
	SELECT DISTINCT
	METRIC_ID,
	'% of student above EOC Civics benchmark' METRIC_NAME,
    NULL as INCIDENT_TYPE,
	NULL as INCIDENT_SUB_TYPE,
	NULL as MEASURE_TYPE,
	NULL as PLACEMENT_TYPE,
	NULL as COLLEGE_NAME,
	NULL as INCIDENT_LOCATION,
	NULL as INCIDENT_TIME,
	NULL as TEACHER_RECOMMENDED_ACTION,
	NULL as ATTENDANCE_CODE,
	NULL as ENTRY_CODE,
	NULL as DROPOUT_CODE,
	TARGET_NUMERATOR,
	TARGET_DENOMINATOR
	FROM BASEDW.DI_KPI_MASTER
	WHERE METRIC_ID = 10030),
	STUDENT_DEMO AS (
	SELECT
                ST.STUDENT_SK,
                ST.STUDENT_ID,
                STUDENT_ETHNICITY,			  
                STUDENT_LANGUAGE_CLASSIFICATION,
                STUDENT_GENDER,
                STUDENT_HOMELESS,
                CURRENT_FLAG,
                STUDENT_ENGLISH_LEARNERS STUDENT_ELL,
                STUDENT_SPECIAL_ABILITY,	  
                STUDENT_ECONOMICALLY_DISADVANTAGED,
                STUDENT_ESE,
                STUDENT_COHORT_GROUP ,
                DEMOGRAPHIC_HASH
	FROM BASEDW.DI_STUDENT_EXT ST),
	ASSESSMENTS AS (
	SELECT DISTINCT 
		'% of student above EOC Civics benchmark' as METRIC_NAME,
		SYEAR,
		S.CUSTOM_327 SCHOOL_ID, 
		S.CUSTOM_100000001 DISTRICT_ID,
		NVL(F.GRADELEVEL,'Not Reported') as STUDENT_GRADE,
		ST.STUDENT_ETHNICITY,
		ST.STUDENT_LANGUAGE_CLASSIFICATION,
		ST.STUDENT_GENDER,
		ST.STUDENT_HOMELESS,
		ST.STUDENT_SPECIAL_ABILITY,
		ST.STUDENT_ECONOMICALLY_DISADVANTAGED,
		ST.STUDENT_ESE,
		ST.STUDENT_COHORT_GROUP,
		ST.STUDENT_ELL,
		ST.DEMOGRAPHIC_HASH,
		NULL as ASSESSMENT_TYPE,
		COUNT(DISTINCT F.STUDENT_ID) TEST_TAKERS,
		COUNT( DISTINCT CASE WHEN F.SCORE >= 3 THEN F.STUDENT_ID ELSE NULL END) proficient_students,
	  COUNT(*) NUM_RECORDS
	FROM BASEDW.FI_STUDENT_ASSESSMENT F 
		 JOIN BASEDW.DI_ASSESSMENT DI ON (F.ASSESSMENT_SK = DI.ASSESSMENT_SK ) 
		 JOIN BASEDW.DI_SCORE_TYPE SCORE ON (SCORE.SCORE_TYPE_SK = F.SCORE_TYPE_SK)
		 JOIN STUDENT_DEMO ST ON (F.STUDENT_ID = ST.STUDENT_ID AND ST.CURRENT_FLAG = 1) 
		 RIGHT OUTER JOIN BASEDW.DI_SCHOOLS S ON (S.SCHOOL_SK = F.SCHOOL_SK AND S.CURRENT_FLAG = 1) 
	WHERE 1=1 
		AND DI.SHORT_NAME = 'ECS'
		AND SCORE.SHORT_NAME = 'AL'
		AND F.SYEAR IS NOT NULL
	GROUP BY
		SYEAR,
		S.CUSTOM_327,
		S.CUSTOM_100000001,	
		F.GRADELEVEL,
		ST.STUDENT_ETHNICITY,
		ST.STUDENT_LANGUAGE_CLASSIFICATION,
		ST.STUDENT_GENDER,
		ST.STUDENT_HOMELESS,
		ST.STUDENT_SPECIAL_ABILITY,
		ST.STUDENT_ECONOMICALLY_DISADVANTAGED,
		ST.STUDENT_ESE,
		ST.STUDENT_COHORT_GROUP,
		ST.STUDENT_ELL,
		ST.DEMOGRAPHIC_HASH)
	SELECT DISTINCT M.METRIC_ID,
		 CY.SYEAR SCHOOL_YEAR,
		 NULL SCHOOL_MONTH,
		 NULL TERM,
		 NULL WEEK,
		 NULL GRADING_PERIOD,
		 CY.SCHOOL_ID,
		 CY.DISTRICT_ID,	 
		 CY.STUDENT_GRADE,
		 CY.STUDENT_ETHNICITY,
		 CY.STUDENT_LANGUAGE_CLASSIFICATION,
		 CY.STUDENT_GENDER,
		 CY.STUDENT_HOMELESS,
		 CY.STUDENT_SPECIAL_ABILITY,
		 CY.STUDENT_ECONOMICALLY_DISADVANTAGED,
		 CY.STUDENT_ESE,
		 CY.STUDENT_COHORT_GROUP,
		 CY.STUDENT_ELL,
		CY.proficient_students METRIC_NUMERATOR,
		CY.TEST_TAKERS METRIC_DENOMINATOR,
		PY1.proficient_students METRIC_NUMERATOR_PY1,
		PY1.TEST_TAKERS METRIC_DENOMINATOR_PY1,
		PY2.proficient_students METRIC_NUMERATOR_PY2,
		PY2.TEST_TAKERS METRIC_DENOMINATOR_PY2,
		M.TARGET_NUMERATOR,
		M.TARGET_DENOMINATOR,
		M.INCIDENT_TYPE,
		M.INCIDENT_SUB_TYPE,
		M.MEASURE_TYPE,
		M.PLACEMENT_TYPE,
		M.COLLEGE_NAME,
		M.INCIDENT_LOCATION,
		M.INCIDENT_TIME,
		M.TEACHER_RECOMMENDED_ACTION,
		CY.ASSESSMENT_TYPE,
		M.ATTENDANCE_CODE,
		M.ENTRY_CODE,
		M.DROPOUT_CODE,
    NULL SCHOOL_MONTH_YYYYMM
	FROM ASSESSMENTS CY LEFT JOIN ASSESSMENTS PY1 ON ((CY.SYEAR - 1)= PY1.SYEAR 
	                                              AND CY.SCHOOL_ID = PY1.SCHOOL_ID
												  AND CY.DISTRICT_ID = PY1.DISTRICT_ID
												  AND CY.STUDENT_GRADE = PY1.STUDENT_GRADE
												  AND CY.DEMOGRAPHIC_HASH = PY1.DEMOGRAPHIC_HASH
												  AND  NVL((CASE WHEN CY.STUDENT_COHORT_GROUP <> 'Not Reported' 
                              THEN  TO_CHAR(SUBSTR(CY.STUDENT_COHORT_GROUP,1,4) - 1 || SUBSTR(CY.STUDENT_COHORT_GROUP,5,8) - 1) 
                               END), 'Not Reported')= PY1.STUDENT_COHORT_GROUP)
						LEFT JOIN ASSESSMENTS PY2 ON ((CY.SYEAR - 2)= PY2.SYEAR 
						                          AND CY.SCHOOL_ID = PY2.SCHOOL_ID
												  AND CY.DISTRICT_ID = PY2.DISTRICT_ID
												  AND CY.STUDENT_GRADE = PY2.STUDENT_GRADE
												  AND CY.DEMOGRAPHIC_HASH = PY2.DEMOGRAPHIC_HASH
												  AND NVL((CASE WHEN CY.STUDENT_COHORT_GROUP <> 'Not Reported' 
                              THEN  TO_CHAR(SUBSTR(CY.STUDENT_COHORT_GROUP,1,4) - 2 || SUBSTR(CY.STUDENT_COHORT_GROUP,5,8) - 2) 
                               END), 'Not Reported') = PY2.STUDENT_COHORT_GROUP)
						JOIN METRIC M ON (CY.METRIC_NAME = M.METRIC_NAME)